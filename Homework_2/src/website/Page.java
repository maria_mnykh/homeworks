package website;

public class Page extends HTMLPage{
	private boolean openChrome;
	private boolean openFF;
	
	public boolean getOpenChrome() {
		return openChrome;
	}
	
	public boolean getOpenFF() {
		return openFF;
	}
	
 	public void setOpenChrome(boolean openChrome) {
 		this.openChrome = openChrome;
 	}
	
 	public void setOpenFF(boolean openFF) {
 		this.openFF = openFF;
 	}
 	
	public Page(String title, String url, String htmlContent, boolean openChrome, boolean openFF) {
		super(title, url, htmlContent);
		this.openChrome = openChrome;
		this.openFF = openFF;
	}

	@Override
	public String getHtmlChrome() {
		if (openChrome) {
			return getHtmlContent();
		}
		else {
			return "The page does not open in Chrome";
		}
	}

	@Override
	public String getHtmlFF() {
		if (openFF) {
			return getHtmlContent();
		}
		else {
			return "The page does not open in Firefox";
		}
	}

	@Override
	public void htmlBuilder(String htmlContent) {
		System.out.println(getHtmlContent() + htmlContent);
		
	}
	
	@Override
	public String toString() {
		return "Page [title = " + getTitle() + ", URL = "
				+ getUrl() + /*", HtmlContent = " + getHtmlContent() + 
                  ", Can be opened in Chrome = " + openChrome +
				", Can be opened in Firefox = " + openFF + */ "]";
	}
	
}
