package website;

public abstract class HTMLPage implements PageActions {
	private String title;
	private String url;
	private String htmlContent;
	
	public String getTitle() {
		return title;
	}
	
	public String getUrl() {
		return url;
	}

	public String getHtmlContent() {
		return htmlContent;
	}
 
	public HTMLPage(String title, String url,String htmlContent) {
		this.title = title; 
		this.url = url;
		this.htmlContent = htmlContent;
	}
	
	public void load() {
		System.out.println("The "+ this.title + " has been loaded");
	}
	
    public void refresh() {
    	    System.out.println("The "+ this.title + " has been refreshed");
	}
    abstract public String getHtmlFF();
    abstract public String getHtmlChrome();
    abstract public void htmlBuilder(String htmlContent);
    
    }
