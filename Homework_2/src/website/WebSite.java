package website;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WebSite {
	private List<Page> pages = new ArrayList<Page>();
	
	public List<Page> getPages() {
  	  return this.pages;
    }
	
 // create new collection of pages
	public void createCollection() {
 	   pages.add(new Page("login","/login","Please enter valid credentials",true,true));
 	   pages.add(new Page("home","/home","Welcome on Homepage",true,true));
 	   pages.add(new Page("forum","/forum","Forum page",true,false));
 	   pages.add(new Page("forum","/forum.ua","Сторінка форуму",true,false));
 	   pages.add(new Page("about","/about","About us",false,true));
 	   pages.add(new Page("about","/about.ua","Про нас",false,true));
 }
 
 // print all pages	
  public void printAllPages() {
 	System.out.println("Collection of all available pages are listed below: ");
 	  for (Page page: pages) {
 		  System.out.println(page);
 	  }
 	  System.out.println();
 }
  
//print pages that may be opened in FF
  public void printFFPages() {
 	System.out.println("Collection of the pages that might be opened in Firefox:");
   for (Page page: pages) {
 	      if(page.getOpenFF()) {
 	    	     System.out.println(page);
 	      }
   }
   System.out.println();
 }
 
//print pages that may be opened in Chrome
 public void printChromePages() {
 	System.out.println("Collection of the pages that might be opened in Chrome:");
   for (Page page: pages) {
 	      if(page.getOpenChrome()) {
 	    	     System.out.println(page);
 	      }
   }
   System.out.println();
 }
 
//print unique titles
 public void printUniuqeTitle() {
 	System.out.println("Uniuqe page titles:");
 	 Set<String> spages = new HashSet<String>();
 	 for (Page page: pages) {
 		 spages.add(page.getTitle());
 	 }
 	 System.out.println(spages); 	 
 }

 //set unique titles
 public Set<String> setSet(){
	 Set<String> spages = new HashSet<String>();
	 for (Page page: pages) {
 		 spages.add(page.getTitle());
 	 }
	 return spages;
 }
 
 // print HtlmBuilder + add some content
 public void printHtlmBuilder(String htmlContent) {
	 System.out.println('\n'+ "Result of htmlBuilder method:");
	 for (Page page: pages) {
		 page.htmlBuilder(htmlContent);
	 }
	 System.out.println(); 
 }
 
 
 //returns Map where key - title, values - list of pages with the set titles
 public Map<String,List<Page>> setMap(Set<String> spages) {
	 Map<String,List<Page>> mpages = new HashMap<String,List<Page>>();
	 String title = "";
	 for (String spage: spages){
	      List<Page> arr = new ArrayList<Page>();
	          for (Page page: pages){
	 		   if(page.getTitle().equals(spage)){
	 			 arr.add(page);
	 			title = page.getTitle().toUpperCase();
	 		 }
	 	   }
	      mpages.put(title, arr); 
	     }
	 return mpages;
 }
 
//print Map and HtmlContent for each map value
public void printMap() {
	System.out.println("Map where key - title, values - list of pages with the set titles:");
		System.out.println(setMap(setSet()));
		System.out.println();
		System.out.println("HtmlContent for each page:" );
		Collection<List<Page>>lpages = setMap(setSet()).values();
		for (List<Page>page: lpages) {
			for(Iterator<Page> iter = page.iterator(); iter.hasNext();)
			{
				System.out.println(iter.next().getHtmlContent());
			}
		}
    } 
}