package task2;

import java.util.*;

public class Task2 {
	
	// set array that consists from n elements
	public static void setArray (List<Integer> arr, int n) {
		for (int i = 0; i<n; i++) {
			arr.add((int) (Math.random()*(n)));
		}
		System.out.println("Initial array: " + arr);
	}
	
   //print the array in reverse order
	public static void reorderArray (List<Integer> arr) {
		System.out.print("Reordered array: [");	 
		for (int i = arr.size()-1; i>=0; i--) {
			if (i!=0)
			{
			System.out.print(arr.get(i) + ", ");
			}
			else {
				System.out.print(arr.get(i));
			}
		}
		System.out.println ("]");	
	}
	
	//remove all duplicate elements from the array
	public static void removeDuplicate (List<Integer> arr) {
		int duplicate;
		for (int i=0; i<=arr.size()-1;i++) {
			duplicate = arr.get(i);
			for (int j = i+1;j<=arr.size()-1; j++) {
			if (arr.get(j) == duplicate) {
				arr.remove(j);
				j = i; 
			} 
			}
		}
		System.out.println ("Array without duplication: "+ arr);	
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	List<Integer> arr= new ArrayList<Integer>();
	setArray(arr,9);
	reorderArray(arr);
	removeDuplicate(arr);
}
}
