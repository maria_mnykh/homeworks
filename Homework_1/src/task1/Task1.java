package task1;

import java.util.ArrayList;
import java.util.List;

public class Task1 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Integer> arr= new ArrayList <Integer>();
		setListArray(arr, 7);
		substutiteElement(arr,1,100); 
	}
	
	// set array that consists from n elements
	public static void setListArray (List<Integer> arr, int n) {
		for (int i = 0; i<n; i++) {
			arr.add((int) (Math.random()*n));
		}
		System.out.println("Initial ArrayList: " + arr);
	}
	//substitute all elements a to element b in the first range of list
	public static void substutiteElement (List<Integer> arr, int a, int b) {
		int n = (int) (arr.size()/2);
		for (int i = 0; i<n; i++) {
			if (arr.get(i) == a) {
				arr.set(i, b);
			}
		}
		System.out.println("Modified ArrayList: "+ arr);
	}
}
