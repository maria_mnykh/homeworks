package task3;

public class Task3 {
	static int arr[] = new int []{9, 7, 0, 7, -77, -77, -9, 10, -78, 90};
	static int length = arr.length; 
	
	// find the minimal element in array
	 public static int findMin() {
		int min = arr[0];
		for (int i = 1; i<=length-1; i++) {
			if (arr[i] <= min) {
				min = arr[i];
			}
		}
		return min;
	}

	//return the arithmetic mean all elements within array
	public static int arithmeticMean() {
		int sum = 0; 
		for (int i = 0; i<=length-1; i++) {
			sum += arr[i];
		}
		return (int)sum/length;
	}
  // substitute the last minimum element in array
	public static void subsituteElement () {
		int position = 0; 
		for (int i = 0 ; i<=length-1; i++) {
			if (arr[i]==findMin()) {
				position = i; 
			}
			else {
				continue;
				}
		}
		arr[position] = arithmeticMean(); 
	}
	
	public static void printArray (String message) {
		System.out.print (message);
		for (int i = 0; i<=length-1; i++) {
			if (i<(length-1)) {
			System.out.print(arr[i] + ", ");
			}
			else {System.out.print(arr[i] +"\r");
			}
			}
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printArray("Initial array is: ");
		System.out.println("Minimum element in the array is= "+ findMin());
		System.out.println("The arithmetic mean is = "+ arithmeticMean());
		subsituteElement();
		printArray("Modified array is: ");
		
	}
}
