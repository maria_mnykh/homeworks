package tast1;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class TestHttpResponce {
	private static final String URL = "http://lits.ua/";
	private OkHttpClient client;
	private SoftAssert sAssert;
	
	@BeforeTest
	public void createInstance() {
	    client = new OkHttpClient();
	    sAssert = new SoftAssert();
	}
	
	@Test
	@Parameters({"contentType", "statusCode", "bodyAttribute"})
	public void testResponse(String contentType, int statusCode, String bodyAttribute) throws IOException {
		Request request = new Request.Builder().url(URL).build();
		Response response = client.newCall(request).execute();
		sAssert.assertEquals(response.body().contentType().toString(), contentType);
		sAssert.assertEquals(response.code(), statusCode);
		sAssert.assertEquals(response.body().string().contains(bodyAttribute), 
				true);
		sAssert.assertAll();
		Reporter.log("Test has been passed", true);
	}
}
