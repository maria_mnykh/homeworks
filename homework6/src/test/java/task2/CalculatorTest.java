package task2;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CalculatorTest {
	private Calculator calc = new Calculator();
	private SoftAssert sAssert = new SoftAssert();
	
	
	@Test(dataProvider ="testNumbers", groups = {"adding"})
	public void testAddNumbers(double firstNumber , double secondNumber) {
		sAssert.assertEquals(calc.addNumbers(firstNumber,secondNumber), firstNumber + secondNumber );
		sAssert.assertEquals(calc.addNumbers(secondNumber,firstNumber), firstNumber + secondNumber );
		sAssert.assertAll();
	}
  
	@Test(enabled = true, groups = {"subtraction"})
	@Parameters({"firstNumber","secondNumber"})
	public void testSubtractNumbers(double firstNumber, double secondNumber) {
		sAssert.assertEquals(calc.subtractNumbers(firstNumber,secondNumber), (firstNumber-secondNumber));
		sAssert.assertNotEquals(calc.subtractNumbers(secondNumber,firstNumber), (firstNumber-secondNumber));
		sAssert.assertAll(); 
		
	}

	@Test(groups = {"multiplication"})
	@Parameters({"firstNumber","secondNumber"})
	public void testMultiplyNumbers(double firstNumber, double secondNumber) {
		sAssert.assertEquals(calc.multiplyNumbers(firstNumber,secondNumber), firstNumber*secondNumber);
		sAssert.assertEquals(calc.multiplyNumbers(secondNumber,firstNumber), firstNumber*secondNumber);
		sAssert.assertAll();
	}
	
	@Test(dataProvider ="testNumbers", groups = {"division"})
	public void testDivideNumbers(double firstNumber , double secondNumber) {
		sAssert.assertEquals(calc.divideNumbers(firstNumber, secondNumber), firstNumber/secondNumber);
		sAssert.assertNotEquals(calc.divideNumbers(secondNumber, firstNumber), firstNumber/secondNumber);
		sAssert.assertAll();
		
	}
	
	@DataProvider(name = "testNumbers")
	public static Object[][] dProvider(){
		return new Object [][] {{9.0,3.0},{8.99,1.0},{-77.9,100.01},{-0.01,5.0},{-3.54,16.0},{98.9, 2.0}}; 
		
	}
	
	@Test(expectedExceptions = ArithmeticException.class, groups = {"division"})
	public void testDivideByZero() {
		Reporter.log("Can not divide by 0", true);
		Assert.assertEquals(calc.divideNumbers(7.8, 0), 0.0);
	}
}
