package task2;

public class Program {

	public static void main(String[] args) { 
		
		Calculator calc = new Calculator();
		System.out.println("5.6 + 0 = " + calc.addNumbers(5.6,0.0));
		System.out.println("2.1 - 9 = " + calc.subtractNumbers(2.1,9.0));
		System.out.println("1.5 * (-9.3) = " + calc.multiplyNumbers(1.5,-9.3));
		System.out.println("6.7 / 0 = " + calc.divideNumbers(6.7,0.0)); 
		
	}

}
