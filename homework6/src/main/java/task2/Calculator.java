package task2;

public class Calculator {
	
	public Calculator() { 
	}
	
	public double addNumbers(double firstNumber,double secondNumber) {
		return firstNumber + secondNumber;
	}

	public double subtractNumbers(double firstNumber,double secondNumber) {
		return (firstNumber - secondNumber); 
	}
	
	public double multiplyNumbers(double firstNumber,double secondNumber){
		return firstNumber * secondNumber;
	}
	
	public double divideNumbers(double firstNumber,double secondNumber) throws ArithmeticException {
		if (secondNumber == 0) {
			throw new ArithmeticException ("Divider can not be 0");
		}
		  return firstNumber / secondNumber;
	}

	
}
