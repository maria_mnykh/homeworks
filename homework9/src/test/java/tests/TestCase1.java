package tests;

import java.util.List;


import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pages.EmailPage;
import pages.HomepageMail;
import pages.PasswordPage;
import pages.SearchMail;
import utility.PropertyLoader;

public class TestCase1 extends BaseTestCase{
	
	private SoftAssert sAssert = new SoftAssert();
	private static final Logger LOG = LoggerFactory.getLogger(TestCase1.class);

	@Test
	public void login() throws InterruptedException {
		LOG.info("Submitting email");
		EmailPage emailPage = new EmailPage(driver);
		PasswordPage passwordPage = emailPage.submitEmailPage(PropertyLoader.loadProperty("email"));
		LOG.info("Submitting password");
	    passwordPage.submitPasswordPage(PropertyLoader.loadProperty("password"));
	}
	
	@Test
	public void verifyExistingLetter() throws InterruptedException {
		HomepageMail homepage = new HomepageMail(driver); 
		SearchMail searhMailPage = homepage.searchByMail("hello");
		LOG.info("Looking for existing mail");
		List<WebElement> searchResult = searhMailPage.getSearchMails();
		//System.out.println(searchResult);
		for(WebElement element:searchResult) {
			//System.out.println(element.getText().toString());
			if (element.isDisplayed()) {
				sAssert.assertEquals(element.getText().toString().toLowerCase().contains("hello"), true);
			}
		}
		sAssert.assertAll();
	}
	
	@Test
	public void verifyNonExistingLetter() throws InterruptedException {
		HomepageMail homepage = new HomepageMail(driver); 
		homepage.crearSearchField();
		SearchMail searhMailPage = homepage.searchByMail("bla");
		LOG.info("Looking for non existing mail");
		List<WebElement> searchResult = searhMailPage.getSearchMails();
		for(WebElement element:searchResult) {
			//System.out.println(element.getText().toString());
			if (!element.isDisplayed()) {
				sAssert.assertNotEquals(element.getText().toString().toLowerCase().contains("bla"), true);
			}
		}
		sAssert.assertAll();
		
	}
	
	
}
