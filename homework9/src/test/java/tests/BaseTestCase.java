package tests;


import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import utility.PropertyLoader;
import webdrivers.WebDrivers;

public class BaseTestCase {

	protected WebDriver driver;
	private static final Logger LOG = LoggerFactory.getLogger(TestCase1.class);
	
	@BeforeTest
	@Parameters({"browserName"})
	public void setUp(String browserName)  {
		try {
			driver = WebDrivers.getInstance(browserName);
			LOG.info("Opening browser`s window");
			driver.get(PropertyLoader.loadProperty("url"));
		} catch (Exception e) {
			System.err.println("You have some problem with driver");
			e.printStackTrace();
		}
	}
	
	
	@AfterTest(alwaysRun = true)
	public void tearDown() {
		LOG.info("Killing web driver");
		WebDrivers.killInstance();
	}
}
