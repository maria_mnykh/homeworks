package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class PasswordPage extends Page{
	
	@FindBy(xpath="//input[@type='password']")
	private WebElement passwordField;
	
	@FindBy(xpath="//span[@class='RveJvd snByac']")
	private WebElement submitButton;

	public PasswordPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public HomepageMail submitPasswordPage(String password) {
		getDriverWait(4).until(ExpectedConditions.visibilityOf(passwordField));
		
		passwordField.sendKeys(password);
		submitButton.click();
		return new HomepageMail(driver);
	}
}
