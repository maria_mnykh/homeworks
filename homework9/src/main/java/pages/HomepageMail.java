package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomepageMail extends Page{

	@FindBy(xpath = "//div[@class='gb_Vf gb_Pf']//input")
	private WebElement searchField;
	
	@FindBy(xpath = "//button[@class='gb_Ff gb_Pf']")
	private WebElement searchButton;
	
	public HomepageMail(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public void crearSearchField() {
	    searchField.clear();
	}
 
	public SearchMail searchByMail(String mail) throws InterruptedException {
		getDriverWait(7).until(ExpectedConditions.visibilityOf(searchField));
		searchField.sendKeys(mail);
		searchButton.click();
		Thread.sleep(5000);
		//getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
		//getDriverWait(4);
		return new SearchMail(driver);
	}
}
