package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
	
	protected WebDriver driver;
	
	public Page(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void refreshPage() {
		driver.navigate().refresh();
	}
	
	public WebDriverWait getDriverWait(int seconds) {
		WebDriverWait wait = new WebDriverWait(driver,seconds);
		return wait;
	}
	
}
