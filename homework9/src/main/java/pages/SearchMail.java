package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchMail extends Page{
	
	@FindBy(css="span.bog>span")
	private List<WebElement> searchResult;

	public SearchMail(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public List<WebElement> getSearchMails() {
		return this.searchResult;
	}
	
}
