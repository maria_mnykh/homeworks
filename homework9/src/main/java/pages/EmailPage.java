package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmailPage extends Page{
	
	@FindBy(xpath="//input[@type='email']")
	private WebElement emailField;
	
	@FindBy(xpath="//span[@class='RveJvd snByac']")
	private WebElement submitButton;
	

	public EmailPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public PasswordPage submitEmailPage(String email) {
		emailField.sendKeys(email);
		emailField.submit();
		submitButton.click();
		return new PasswordPage(driver);
	}
	
}
