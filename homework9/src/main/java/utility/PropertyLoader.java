package utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {

	private static Properties properties = new Properties();
	private static final String FILE = "src/main/resources/config.properties";
	
	static {
		try {
			InputStream input = new FileInputStream(FILE);
			properties.load(input);
		} catch (FileNotFoundException e) {
			System.err.println("Make sure that file do exists");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String loadProperty(String propertyName) {
		String property = properties.getProperty(propertyName);
		if (property==null) {
			return "";
		}
		else {
			return property;
		}
	}
}
