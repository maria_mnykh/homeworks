package webdrivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

//import io.github.bonigarcia.wdm.WebDriverManager;

public class WebDrivers {
	
	private static WebDriver driver;
	private static final String CHROME ="Chrome";
	private static final String FIREFOX ="Firefox";
	
	private WebDrivers() {
		
	}

	public static WebDriver getInstance(String browser) throws Exception {
		if (driver==null)
		{
			if (CHROME.equals(browser)){
				setUpChrome();
				driver = new ChromeDriver();
			}
			else if (FIREFOX.equals(browser)) {
				setUpFF();
				driver = new FirefoxDriver();
			}
			else {
				throw new Exception("Invalid browser`s name");
			}
		}
		return driver;
	}
	
	public static void setUpChrome() {
		WebDriverManager.chromedriver().setup();
	}
	
    public static void setUpFF() {
      	WebDriverManager.firefoxdriver().setup();
	}
    
    public static void killInstance() {
    	   if (driver!=null) {
    		   driver.quit();
    		   driver=null;
    	   }
    }
 
}
