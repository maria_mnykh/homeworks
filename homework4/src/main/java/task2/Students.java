package task2;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Students {
	
	private List<Student> students;

	@JsonProperty("students")
	public List<Student> getStudents() {
		return students;
	}

	@Override
	public String toString() {
		return "Students are: " + "\n" + students + "]";
	}
	
}
