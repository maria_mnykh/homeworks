package task2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Student {
	
	@JsonProperty("id")
	private int StudentId; 
	
	@JsonProperty("course")
	private String course;
	
	@JsonProperty("hasDoneHomework")
	private boolean hasDoneHomework;
	
	@JsonProperty("Person")
	private Person person;
	
	public int getId() {
		return StudentId;
	}
	
	public String getCourse() {
		return course;
	}
	
	public boolean isHasDoneHomework() {
		return hasDoneHomework;
	}

	public Person getPerson() {
		return person;
	}

	@Override
	public String toString() {
		return "Student [StudentId=" + StudentId + ", course=" + course + ", hasDoneHomework=" + hasDoneHomework + ", person="
				+ person + "]" +"\n";
	}

}
