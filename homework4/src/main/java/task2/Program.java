package task2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class Program {

	public static final String JSON_FIlE = "src/main/resources/students.json";
	
	
	public static void main(String[] args) {
		try {
			parseJSONToObject();
		}
		catch(FileNotFoundException e) {
		    //e.printStackTrace();
		    System.out.println("Path to file is incorrect or file does not exist");
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void parseJSONToObject() throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
		TypeFactory typeFactory = TypeFactory.defaultInstance();
		try {
			List<Student> students= mapper.readValue(new File(JSON_FIlE), typeFactory.constructCollectionType(LinkedList.class, Students.class));
			System.out.println(students);
		}
		catch (JsonParseException e) {
			e.printStackTrace();
		} 
		catch (JsonMappingException e) {
			e.printStackTrace();
		} 
		
	}

}
