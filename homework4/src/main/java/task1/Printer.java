package task1;

public class Printer {
	
	private int code;
	private String color;
	private String type;
	private double price;
	
	public Printer(int code, String color, String type, double price){
		this.code = code;
		this.color = color;
		this.type = type;
		this.price = price;
	}
	
	public Printer() {
		
	}
	
	public int getCode() {
		return code;
	}
	
	public String getColor() {
		return color;
	}
	
	public String getType() {
		return type;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Printer [code=" + code + ", color=" + color + ", type=" + type + ", price=" + price + "]";
	}

	
}
