package task1;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Program {

	public static final String FILE = "src/main/resources/config.properties";
	public static Connection connection;
	public static Statement statement;
	public static ResultSet result;
	public static List<Product> productList;
	public static List<Printer> printerList;
	
	public static void main(String[] args) {
		try {
			createDBConnection();
			readProducts();
			readPrinters();
			result.close();
			statement.close();
			connection.close();
		}
		catch(SQLException e) { 
			System.out.println("You have some problem with data base");
			e.printStackTrace();
		}
		finally {
			if (!(result==null)) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (!(statement==null)) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (!(connection==null)) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void createDBConnection() throws SQLException {
		try {
			Class.forName(readConfigFromFile("driver"));
		} catch (ClassNotFoundException e) {
			System.out.println("Path to file is incorrect or it may not exists");
			e.printStackTrace();
		}
			connection = DriverManager.getConnection(readConfigFromFile("url"), 
					readConfigFromFile("user"), readConfigFromFile("password"));
	}
	
	public static String readConfigFromFile(String configName) {
		InputStream input = null; Properties pr = null;
		try {
		  pr = new Properties();
		  input = new FileInputStream(FILE);
		  pr.load(input);
		  
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if (!(input==null)) {
				try {
					input.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return pr.getProperty(configName);
	}
	
	public static ResultSet executeQuery(String query) throws SQLException {
		statement = connection.createStatement();
		return statement.executeQuery(query);
		
	}
	
	public static void readProducts() throws SQLException {
		
		result = executeQuery("SELECT * FROM ShopShema.product WHERE type='laptop'");
		productList= new ArrayList<Product>();
		while(result.next()) {
			productList.add(new Product(result.getString(1),result.getString(2),result.getString(3)));
		}
		System.out.println(productList);
		
	}
	
	public static void readPrinters() throws SQLException {
		
		printerList = new ArrayList<Printer>();
		result = executeQuery("SELECT * FROM ShopShema.printer WHERE price>=700.0");
		while(result.next()) {
			printerList.add(new Printer(result.getInt(1),result.getString(2),
					result.getString(3), result.getDouble(4)));
		}
		System.out.println(printerList);
	}
	
	}
