package task2;

import java.time.LocalDate;

public class Task2 {

	public static void calculateDate(LocalDate date, long years, long months, long days) {
		
		System.out.println("Taday`s date is "+ date);
	    LocalDate futureDate = date.plusDays(days).plusMonths(months).plusYears(years);
		System.out.println("Future date will be " + futureDate);
	}
	
	public static void main(String[] args) {
		
		calculateDate(LocalDate.now(), 2, 8, 6);
	}
}
