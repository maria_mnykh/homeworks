package task3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Task3 {
	
	public static final String FILE_TO_WRITE = "textToWrite.txt";
	public static final String FILE_TO_READ = "text.txt";
	
	//read map from file
	public static Map<Integer,String> readFromFile() {
		
		File file  = new File(FILE_TO_READ);
		Map<Integer,String> mapFile = new HashMap<Integer,String>();
		Scanner scanner = null;
		
		 try {
		     if(file.exists()) {
				scanner = new Scanner(file);
				for (int i = 1;scanner.hasNextLine(); i++) {
					mapFile.put(i, scanner.nextLine());
				}
		     }
			} 
		 catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		 
		 finally {
			 if(scanner!=null){
			 scanner.close();
			 }
		 }
		return mapFile;
	}
	
	// find keys that are a degree of 2 and return them as list
	public static List<Integer> convertMapToList(Map<Integer,String> map) {
		
		List<Integer>list = new ArrayList<Integer>(map.keySet());
		for (int i = 0; i<=list.size()-1;i++)
		{
			if(!(list.get(i).equals((int)Math.pow((int)2, (int)i)))) { 
				list.remove(i);
				  i--;
			}
		}
		return list;
	}
	
	//write list of keys to file
	public static void writeToFile(List<Integer>list)  {
		
		BufferedWriter bw = null;
		
		try {
		    bw = new BufferedWriter(new FileWriter(FILE_TO_WRITE));
		    for (int i = 0; i<=list.size()-1; i++)
		      	bw.write(list.get(i).toString() + "\n");
		}
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		catch(IOException e){
			e.printStackTrace();
		}
		
		finally{
			try {
				if(bw!=null) {
				bw.close();
				}
			} 
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
    public static void main(String[] args) {
		
		System.out.println("HashMap of keys and values taken from text.txt file \n" + readFromFile());
		System.out.println("Keys that are a degree of number 2 \n" + convertMapToList(readFromFile()));
		// write keys to file
		writeToFile(convertMapToList(readFromFile()));

	}
 }
