package task1;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Task1 {
	
    public static final String LETTERS_LOW = "qazxswedcvfrtgbnhyujmkiolop";
    public static final String NUMBERS ="0123456789";
    public static final String LETTERS_UP = "ZAQWSXCDERFVBGTYHNMJUIKLOP";
    public static final String SYMBOLS = "~!@#$%^&*()_+=|";
    
	public static void main(String[] args) {
		System.out.println("Random collection of strings: \n" + generateString(9));
	}
	
	public static List<StringBuilder> generateString(int numberString) {
		List<StringBuilder> list = new LinkedList<StringBuilder>();
		Random random = new Random(); 
		String joinedString = LETTERS_LOW.concat(LETTERS_UP).concat(NUMBERS).concat(SYMBOLS);
		for (int i=0;i<=numberString-1;i++){
			int numberDigits = random.nextInt(15 - 6) + 6;
			//System.out.println(numberDigits);
			StringBuilder randomString = new StringBuilder();
			while (randomString.length()<numberDigits) {
				int randomIndex = random.nextInt(joinedString.length());
				char randomChar = joinedString.charAt(randomIndex);
				randomString.append(randomChar);
			}
			list.add(randomString);
		}
     return list;
	}
}
